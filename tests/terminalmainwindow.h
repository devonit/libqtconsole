#ifndef TERMINALMAINWINDOW_H
#define TERMINALMAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class TerminalMainWindow;
}

class TerminalMainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit TerminalMainWindow(QWidget *parent = 0);
    ~TerminalMainWindow();
    
private slots:
    void on_actionExit_triggered();

private:
    Ui::TerminalMainWindow *ui;
};

#endif // TERMINALMAINWINDOW_H
