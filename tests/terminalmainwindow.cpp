#include "terminalmainwindow.h"
#include "ui_terminalmainwindow.h"

#include <QFont>

TerminalMainWindow::TerminalMainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::TerminalMainWindow)
{
    ui->setupUi(this);

    QFont font = QApplication::font();
    font.setFamily("Monospace");
    font.setPointSize(8);

    ui->console->setTerminalFont(font);
    connect(ui->console, SIGNAL(finished()), this, SLOT(close()));
}

TerminalMainWindow::~TerminalMainWindow()
{
    delete ui;
}

void TerminalMainWindow::on_actionExit_triggered()
{

}
