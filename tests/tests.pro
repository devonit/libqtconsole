QT       += core gui

TARGET = tests
TEMPLATE = app

SOURCES += main.cpp\
           terminalmainwindow.cpp

HEADERS  += terminalmainwindow.h
FORMS    += terminalmainwindow.ui

LIBS += -L$$OUT_PWD/../lib -lqtconsole
INCLUDEPATH += $$PWD/../lib
DEPENDPATH += $$OUT_PWD/../lib $$PWD/../lib
